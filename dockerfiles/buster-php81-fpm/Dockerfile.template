FROM {{ "buster-php81" | image_tag }} AS buster-php80

# We don't want the default index.html for running PHP applications, and
# xdebug is a major performance drag, so disable it here (it's already
# been disabled for CLI PHP in buster-php72).

RUN {{ "php8.1-fpm" | apt_install }}

# php-fpm configuration:
COPY php-fpm.conf /etc/php/8.1/fpm/php-fpm.conf
RUN cp /docker/www.conf /etc/php/8.1/fpm/pool.d/www.conf

# MediaWiki-specific php.ini settings
# upload_max_filesize and post_max_size here match production, per T246930
RUN echo 'opcache.file_update_protection=0\n\
opcache.memory_consumption=256\n\
opcache.max_accelerated_files=24000\n\
opcache.max_wasted_percentage=10\n\
zlib.output_compression=On\n\
upload_max_filesize=100M\n\
post_max_size=100M\n' | tee -a /etc/php/8.1/fpm/php.ini /etc/php/8.1/cli/php.ini

RUN cp --force /docker/xdebug.ini "/etc/php/$PHP_VERSION/fpm/conf.d/20-xdebug.ini"

EXPOSE 9000

WORKDIR "/var/www/html/w"

ENTRYPOINT ["/bin/bash", "/php_entrypoint.sh"]
